<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
		<title>404 Error | Page Not Found</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/logo/favicon.png') }}">

        <!-- plugins css -->
        <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/dist/css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendors/perfect-scrollbar/css/perfect-scrollbar.min.css') }}">

        <!-- core css -->
        <link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
	</head>

	<body>
		<div class="app">
			<div class="authentication">
				<div class="page-404 container">
					<div class="row">
						<div class="col-md-6">
							<div class="full-height">
								<div class="vertical-align full-height pdd-horizon-70">
									<div class="table-cell">
										<h1 class="text-dark font-size-80 text-light">Opps!</h1>
										<p class="lead lh-1-8">Hello there, You seem to be lost, but don't worry,<br>we'ill get you back on track...</p>
										<button onclick="return history.back()" class="btn btn-info">Get Me Back!</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-5 ml-auto hidden-sm hidden-xs">
							<div class="full-height height-100">
								<div class="vertical-align full-height">
									<div class="table-cell">
										<img class="img-responsive" src="{{ asset('assets/images/others/404.png') }}" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
