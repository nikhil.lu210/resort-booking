<!-- Footer START -->
<footer class="content-footer">
    <div class="footer">
        <div class="copyright go-right">
            <span>Copyright © 2019 <b class="text-primary">Restaurent_Name</b>. All rights reserved || Developed By <b><a href="#" class="text-info">Veechi Technologies</a></b></span>
            {{-- <span class="go-right">
                <a href="#" class="text-gray mrg-right-15">Term &amp; Conditions</a>
                <a href="#" class="text-gray">Privacy &amp; Policy</a>
            </span> --}}
        </div>
    </div>
</footer>
<!-- Footer END -->
