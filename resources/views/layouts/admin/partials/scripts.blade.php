<script src="{{ asset('assets/js/vendor.js') }}"></script>

@yield('script_links')

<script src="{{ asset('assets/js/app.min.js') }}"></script>

<script src="{{ asset('assets/js/script.js') }}"></script>
<script src="{{ asset('assets/js/responsive.js') }}"></script>

@yield('custom_script')
