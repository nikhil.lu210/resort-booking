<?php

Auth::routes();

Route::get('/', 'Admin\Dashboard\DashboardController@index');


/*===================================
===========< Admin Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth'],
    ],
    function () {
        include_once 'admin/admin.php';
    }
);
